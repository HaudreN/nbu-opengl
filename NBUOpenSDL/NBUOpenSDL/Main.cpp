#include <iostream>
#include <vector>
#include <string>
#include <GL/glew.h>
#include "Display.h"
#include "Mesh.h"
#include "Shader.h"
#include "Texture.h"
#include "Transform.h"
#include "Camera.h"
using namespace std;
using namespace glm;


int main(int argc, char** argv)
{
	Display display(800, 600, "NBU PROJECT");
	float displaySettings= 800.0f / 600.0f;
	Vertex vertices[] = { Vertex (vec3(-0.5,-0.5,0), vec2(0.0 ,0.0)),
						Vertex (vec3(0,0.5,0),vec2(0.5,1.0)),
							Vertex (vec3(0.5,-0.5,0),vec2(1.0,0.0) ) };

	unsigned int indices[] = { 0,1,2 };

	Mesh mesh(vertices, sizeof(vertices) / sizeof(vertices[0]), indices, sizeof(indices) / sizeof(indices[0]));
	Mesh mesh2("CasaMedievalC.obj");
	Shader shader("basicShader");
	Texture texture("wood.jpg");
	Camera camera(glm::vec3(0, 0, -20), 70.0f, displaySettings, 0.1f, 1000.0f);
	Transform transform;

	float counter = 0.0f;
	while (!display.notRunning())
	{
	
			display.Clear(0.0f, 0.15f, 0.3f, 1.0f);
		
		float sinCounter = sinf(counter);
		float cosCounter = cosf(counter);
		transform.GetPos().x = sinCounter;
		transform.GetPos().z = cosCounter;
		transform.GetRot().x = counter;
		transform.GetRot().y = counter ;
		transform.GetRot().z = counter;
		transform.SetScale( glm::vec3(cosCounter, cosCounter, cosCounter));
		shader.Bind();
		texture.Bind(0);
		shader.Update(transform, camera);
		mesh2.Draw();
		display.Update();
		counter += 0.01f;
	}

	return 0;

}