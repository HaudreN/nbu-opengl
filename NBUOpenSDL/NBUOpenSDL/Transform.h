#pragma once
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
using namespace glm;
class Transform
{
public:
	Transform(const vec3& pos = vec3(), const vec3& rot = vec3(), const vec3& scale = vec3(1.0f, 1.0f, 1.0f))
	{
		this->m_pos = pos;
		this->m_rot = rot;
		this->m_scale = scale;
	}
	inline mat4 GetModel() const
	{
		mat4 posMatrix = translate(m_pos);
		mat4 rotXMatrix = rotate(m_rot.x, glm::vec3(1, 0, 0));
		mat4 rotYMatrix = rotate(m_rot.y, glm::vec3(0, 1, 0));
		mat4 rotZMatrix = rotate(m_rot.z, glm::vec3(0, 0, 1));
		mat4 scaleMatrix = translate(m_scale);

		mat4 rotMatrix = rotZMatrix * rotYMatrix * rotXMatrix;

		return posMatrix * rotMatrix * scaleMatrix;
	}
	inline void SetPos(const glm::vec3& pos) { m_pos = pos; }
	inline void SetRot(const glm::vec3& rot) { m_rot = rot; }
	inline void SetScale(const glm::vec3& scale) { m_scale = scale; }
	inline vec3& GetPos() { return m_pos; }
	inline vec3& GetRot() { return m_rot; }
	inline vec3& GetScale() { return m_scale; }
	virtual ~Transform();
private:
	glm::vec3 m_pos;
	glm::vec3 m_rot;
	glm::vec3 m_scale;

};

