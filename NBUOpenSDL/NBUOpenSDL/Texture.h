#pragma once
#include <string>
#include <GL/glew.h>
#include "Texture.h"
#include <iostream>
class Texture
{
public:
	Texture(const std::string& fileName);

	void Bind(unsigned int unit) ;
	virtual ~Texture();
private:
	GLuint m_texture;

	
};

