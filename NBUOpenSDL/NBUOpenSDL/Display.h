#pragma once
#include <string>
#include <SDL2/SDL.h>
using namespace std;
class Display
{
public:
Display(int width, int height, const string& title);
void Update();
virtual ~Display();
bool notRunning();
void Clear(float r, float g, float b, float a);

protected:
private:
	Display(const Display& other) {}

	SDL_Window*  m_window;
	SDL_GLContext m_glContext;
	bool m_isClosed;
};

