#include "Texture.h"
#include "stb_image.h"
Texture::Texture(const std::string& fileName)
{
	 int width, height, numComponents;
	unsigned char* imageData =stbi_load(fileName.c_str(), &width, &height, &numComponents, 4);

	if (imageData == NULL)
	{
		std::cerr << "Cannot load Texture" << fileName << std::endl;
	}
	glGenTextures(1, &m_texture);
	glBindTexture(GL_TEXTURE_2D, m_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//for when texture takes fewer pixels or more pixels than whats on the screen
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,width,height,0,GL_RGBA,GL_UNSIGNED_BYTE, imageData);
	stbi_image_free(imageData);
}
void Texture::Bind(unsigned int unit)
{
	glBindTexture(GL_TEXTURE_2D, m_texture);
	
}
Texture::~Texture()
{
	glDeleteTextures(1, &m_texture);
}